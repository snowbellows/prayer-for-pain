// @ts-ignore-file

function withRatio<T extends (...args: number[]) => any>(
  fn: T,
  ...args: number[]
): () => ReturnType<T> {
  return () => { return fn(
    ...args.map((arg, index) => (index % 2 === 0 ? xRatio(arg) : yRatio(arg)))
  )};
}

    withRatio(p.line, 0.35, 0.13, 0.454, 0.86)()
