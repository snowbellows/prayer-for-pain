# A Prayer for Pain

## Overview

Uses my personal template for making a p5js sketch using Snowpack and Typescript.

## Prerequisites

```sh
npm install
```

## Development

To work on the sketch run the dev server using:

```sh
npm run start
```

This will start the local development server on `http://localhost:8080/`.

The p5js code lives in index.ts in the `setup` and `draw` functions:

```js
p.setup = () => {
  p.createCanvas(window.innerWidth, window.innerHeight);
  p.background(200);
};

p.draw = () => {
  p.circle(50, 50, 50);
};
```

## Build and Deployment

This project doesn't yet have a build and deployment but you can run

```sh
npm run build
```

This will compile your Typescript and dependencies into an html file and bundle in the `build` directory.

But it still needs to be served over HTTP. You can do this locally using <https://github.com/http-party/http-server>
