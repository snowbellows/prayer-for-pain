import _ from 'lodash';
import p5 from 'p5';

interface Colour {
  h: number;
  s: number;
  b: number;
}
interface Line {
  x1: number;
  y1: number;
  x2: number;
  y2: number;
}
interface Bezier {
  x1: number;
  y1: number;
  cx1: number;
  cy1: number;
  cx2: number;
  cy2: number;
  x2: number;
  y2: number;
  colour: Colour;
}

const red: Colour = { h: 358, s: 90, b: 76 };
const acceleration = 0.03;
const numPairs = 500;

const sketch = (p: p5) => {
  let line1: Line;
  let line2: Line;
  let bezierCurves: Bezier[][] = [];

  p.setup = () => {
    // Run at start
    p.colorMode(p.HSB);

    p.frameRate(30);
    p.createCanvas(window.innerWidth, window.innerHeight);
    p.background(red.h, red.s, red.b);

    line1 = {
      x1: xRatio(p, 0.35),
      y1: yRatio(p, 0.13),
      x2: xRatio(p, 0.454),
      y2: yRatio(p, 0.96),
    };

    line2 = {
      x1: xRatio(p, 0.55),
      y1: yRatio(p, 0.18),
      x2: xRatio(p, 0.448),
      y2: yRatio(p, 0.965),
    };
    bezierCurves = populateBezierPairs(p, numPairs, { line1, line2 });
  };

  p.draw = () => {
    // Runs every frame
    p.background(red.h, red.s, red.b, 0.1);

    p.noFill();

    moveBeziers(p, bezierCurves, { line1, line2 });

    bezierCurves.forEach((b, index) => drawBeziers(p, b, index));
    p.stroke(0, 0, 100);
    p.strokeWeight(10);
    p.strokeCap(p.PROJECT);
    p.line(line1.x1, line1.y1, line1.x2, line1.y2);
    p.line(line2.x1, line2.y1, line2.x2, line2.y2);
  };

  p.mousePressed = () => {
    p.fullscreen(true);
    p.createCanvas(window.innerWidth, window.innerHeight);
  };
};

function xRatio(pp: p5, ratio: number): number {
  return ratio * pp.width;
}

function yRatio(pp: p5, ratio: number): number {
  return ratio * pp.height;
}

function noisePlusMinus(pp: p5, value: number) {
  return (pp.noise(value) - 0.5) * 2;
}

function populateBezierPairs(
  pp: p5,
  pairs: number,
  lines: { line1: Line; line2: Line }
): Bezier[][] {
  let bezierPairs: Bezier[][] = [];
  for (let index = 0; index < pairs; index++) {
    pushBezierPair(pp, bezierPairs, lines, index);
  }
  return bezierPairs;
}

function pushBezierPair(
  pp: p5,
  beziers: Bezier[][],
  { line1, line2 }: { line1: Line; line2: Line },
  index: number
): void {
  const colour = {
    h: red.h + 5 * noisePlusMinus(pp, index * acceleration),
    s: red.s + 10 * noisePlusMinus(pp, (index + 20) * acceleration),
    b: red.b * pp.noise((index + 40) * acceleration),
  };

  const b1 = {
    x1: ((pp.noise(index) * pp.width) / 3) * 2,
    y1: pp.noise(index + 10) * pp.height,
    cx1: line1.x1,
    cy1: line1.y1,
    cx2: line1.x2,
    cy2: line1.y2,
    x2: line2.x2 + (line1.x2 - line2.x2) / 2,
    y2: yRatio(pp, 2),
    colour,
  };

  const b2 = {
    x1: ((pp.noise(index) * pp.width) / 3) * 2 + pp.width / 3,
    y1: pp.noise(index + 10) * pp.height,
    cx1: line2.x1,
    cy1: line2.y1,
    cx2: line2.x2,
    cy2: line2.y2,
    x2: line2.x2 + (line1.x2 - line2.x2) / 2,
    y2: yRatio(pp, 2),
    colour,
  };

  beziers.push([b1, b2]);
}

function moveBeziers(
  pp: p5,
  bezierPairs: Bezier[][],
  lines: { line1: Line; line2: Line }
): void {
  bezierPairs.shift();
  pushBezierPair(pp, bezierPairs, lines, pp.frameCount);
  for (let index = bezierPairs.length - 1; index >= 0; index--) {
    const element = bezierPairs[index];
    const velocity = index * acceleration * pp.noise(pp.frameCount);

    moveBezierPair(element, velocity);
  }
}

function moveBezierPair(bezierPair: Bezier[], velocity: number): void {
  const [b1, b2] = bezierPair;

  // b1
  bezierPair[0].x1 = b1.x1 - velocity;
  bezierPair[0].y1 = b1.y1 - velocity;
  // bezierPair[0].cx1 = b1.cx1 - velocity;
  bezierPair[0].cy1 = b1.cy1 - velocity;
  // bezierPair[0].cx2 = b2.cx2 - velocity;
  bezierPair[0].cy2 = b1.cy2 - velocity;
  // bezierPair[0].x2 = b1.x1 - velocity;
  bezierPair[0].y2 = b1.y2 - velocity;

  // b2
  bezierPair[1].x1 = b2.x1 + velocity;
  bezierPair[1].y1 = b2.y1 - velocity;
  // bezierPair[1].cx1 = b2.cx1 + velocity;
  bezierPair[1].cy1 = b2.cy1 - velocity;
  // bezierPair[1].cx2 = b2.cx2 + velocity;
  bezierPair[1].cy2 = b2.cy2 - velocity;
  // bezierPair[1].x2 = b2.x1 + velocity;
  bezierPair[1].y2 = b2.y2 - velocity;
}

function drawBeziers(pp: p5, beziers: Bezier[], index: number) {
  beziers.forEach((b) => {
    pp.stroke(b.colour.h, b.colour.s, b.colour.b, index / numPairs);
    // pp.fill(b.colour.h, b.colour.s, b.colour.b, index / numPairs);
    // pp.strokeWeight(2)
    // pp.strokeWeight(5)
    // pp.strokeWeight(50)

    pp.strokeWeight(50 * pp.noise(pp.frameCount));
    pp.bezier(b.x1, b.y1, b.cx1, b.cy1, b.cx2, b.cy2, b.x2, b.y2);
  });
}

const P5 = new p5(sketch);
